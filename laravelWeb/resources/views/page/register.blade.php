<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Form</title>
  </head>
  <body>
    <h1>Buat Account Baru</h1>
    <h4>Sign Up Form</h4>
    <form>
      <label for="fname">First name:</label><br />
      <input type="text" id="fname" name="fname" /><br />
      <label for="lname">Last name:</label><br />
      <input type="text" id="lname" name="lname" />
    </form>

    <p>Gender</p>

    <form action="./welcome.html">
      <input type="radio" id="male" name="fav_language" value="gender male" />
      <label for="html">Male</label><br />
      <input
        type="radio"
        id="female"
        name="fav_language"
        value="gender female"
      />
      <label for="css">Female</label><br />

      <p>Nationality</p>
      <select id="cars" name="cars">
        <option value="idn">Indonesia</option>
        <option value="mly">Malaysia</option>
        <option value="ida">India</option>
        <option value="chn">China</option>
      </select>
      <p>Language Spoken</p>
      <input type="checkbox" id="lang1" name="language1" value="bindo" />
      <label for="vehicle1">Bahasa Indonesia</label><br />
      <input type="checkbox" id="lang2" name="language2" value="english" />
      <label for="vehicle2">English </label><br />
      <input type="checkbox" id="lang3" name="language3" value="other" />
      <label for="vehicle3">Other</label>
      <p>Bio</p>
      <textarea name="message" rows="10" cols="30"></textarea> <br />

      <input type="submit" value="Sign Up" />
    </form>
  </body>
</html>
